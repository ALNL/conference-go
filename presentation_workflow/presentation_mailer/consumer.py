import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    new_body = json.loads(body)
    send_mail(
        'Your presentation has been accepted',
        f'{new_body["presenter_name"]}, we are happy to tell you that your presentation {new_body.["title"]} has been accepted',
        'admin@conference.go',
        [new_body["presenter_email"]],
        fail_silently=False,
    )

def process_rejection(ch, method, properties, body):
    new_body = json.loads(body)
    send_mail(
        'Your presentation has been rejected',
        f'{new_body["presenter_name"]}, we are sorry to tell you that your presentation {new_body["title"]} has been accepted',
        'admin@conference.go',
        [new_body["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )

        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
            )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Cound not connect to RabbitMQ")
        time.sleep(2.0)
